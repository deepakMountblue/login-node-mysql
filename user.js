const router = require('express').Router()
const pool = require('./db')

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.post('/login', (req, res) => {

    if (req.body.password && req.body.email) {
        var email = req.body.email;
        var password = req.body.password;
        var sql = 'SELECT * FROM users WHERE email =? AND password =?';
        if (validateEmail(email)) {
            pool.getConnection((err1, db) => {
                if (err1) {
                    next(err1);
                } else {
                    db.query(sql, [email, password], function (err, data, fields) {
                        if (err) {
                            next(err)
                        }
                        else if (data.length > 0) {
                            req.session.login = true;
                            req.session.email = email;
                            res.redirect('/');
                        } else {
                            res.render('login', { alertMsg: "Your Email Address or password is wrong" });
                        }
                    })
                }
            });
        }
        else {
            res.render('login', { alertMsg: "not a valid email!" })
        }
    }
    else {
        res.render('login', { alertMsg: "email and password are required" })
    }
})

router.post('/register', (req, res, next) => {

    const inputData = {
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        password: req.body.password,
    }
    console.log(inputData)
    var sql = 'SELECT * FROM users WHERE email =?';
    if (!inputData.fname || !inputData.lname || !inputData.email || !inputData.password){
        res.render('register', { alertMsg: "all fields are required!" });
    }
    else{
        pool.getConnection((err1, db) => {
            if (err1) {
                next(err1);
            } else {
                if(!validateEmail(inputData.email)){
                    res.render('register', { alertMsg: "NOt a valid email!" });
                }
                else{
                    db.query(sql, [inputData.email], function (err, data, fields) {
                        if (err) {
                            next(err)
                        }
                        else if (data.length > 1) {
                            // check unique email address
                            var msg = inputData.email + "was already exist";
                            res.render('register', { alertMsg: msg });
                        }
                        else if (inputData.password === undefined) {
                            var msg = "Password should not be empty";
                            res.render('register', { alertMsg: msg });
                        }
                        else if (req.body.confirm_password != inputData.password) {
                            var msg = "Passwords not Matched";
                            res.render('register', { alertMsg: msg });
                        }
                        else {
                            // save users data into database
                            var sql = 'INSERT INTO users SET ?';
                            db.query(sql, inputData, function (err, data) {
                                if (err) {
                                    next(err)
                                }
                                else {
                                    var msg = "Your are successfully registered";
                                    res.render('register', { alertMsg: msg });
                                }
                            });
                        }
    
                    })
                }
            }
        });
    }
})

router.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/')
})

module.exports = router