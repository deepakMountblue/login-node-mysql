const mysql = require('mysql');

const {
  host, user, database, password,
} = require('./config').db;

const pool = mysql.createPool({
  host,
  user,
  password,
  database,
});

module.exports = pool;
