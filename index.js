const express = require('express');
const path = require('path')
const app = express();
const { port } = require('./config').app;

const bodyParser = require('body-parser')
const session = require('express-session')
const router = require('./user')
const pool = require('./db')
app.set('view engine', 'ejs');

app.use('/', express.static(`${__dirname}/views`));
app.use('/api', express.static(`${__dirname}/views`));
app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.use(session({
    secret: '123456cat',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }
}))

app.use('/api', router)

app.get('/', (req, res) => {
    if (req.session.login) {
        res.render('index', { email: req.session.email })
    }
    else {
        res.redirect('/login')
    }
})

app.get('/register', (req, res) => {
    if(req.session.login){
        res.redirect('/')
    }
    else{
        res.render('register');
    }
    
});

app.get('/login', (req, res) => {
    if(req.session.login){
        res.redirect('/')
    }
    else{
        res.render('login')
    }
    
})


const fetchQuery = (query, res, next) => {
    pool.getConnection((err, db) => {
        if (err) {
            next(err);
        } else {
            db.query(query, (error, rows) => {
                if (error) {
                    next(error);
                } else {
                    res.status(200)
                        .json(rows).end();
                }
            });
        }
    });
};
app.get('/addData', (req, res, next) => {
    var sql = "INSERT INTO users (fname, lname,email,password) VALUES ?";
    var values = [
        ['deepak','paliwal','deepak@gmail.com','1234'],
        ['ankit','sharma','ankit@gmail.com','5555'],
        ['akash','mishra','akash@gmail.com','3456'],
        ['bhupendra','chauhan','bhup@gmail.com','1111']
      ];
    pool.getConnection((err, db) => {
        if (err) {
            next(err);
        } else {
            db.query(sql, [values], function (error, result) {
                if (error) {
                    next(error);
                } else {
                    res.status(200)
                        .json(result).end();
                }
            });
        }
    });


})

app.get('/query', (req, res, next) => {
    var sql = `create table users(
        id int(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
        fname varchar(30) DEFAULT NULL,
        lname varchar(30) DEFAULT NULL,
        email varchar(50) DEFAULT NULL,
        password varchar(20) DEFAULT NULL
    )`;

  fetchQuery(sql,res,next)

})

app.use((req, res, next) => {
    const error = new Error('Page Not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = err;

    res.status(err.status || 500).send(err.message).end();
});

app.listen(port, () => {
    console.log(`listening to port ${port}`);
});


