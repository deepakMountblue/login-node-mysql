require('dotenv').config();

const production = {
  app: {
    port: process.env.PORT || 5000,
  },
  db: {
    host: process.env.HOST,
    user: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
  },
};

const test = {
  app: {
    port: process.env.PORT || 8000,
  },
  db: {
    host: process.env.HOST,
    user: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
  },
};

const config = {
  production, test,
};

module.exports = config[process.env.NODE_ENV];
